Arduino PC104 Developement board 

A PC-104 board (Cubesat standard) to quickly prototype ideas using the convenience of the Arduino platform. It also includes a CAN transceiver for interacting with other nodes, according to LibreCube specifications.